package com.walking.techie.properties;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.Set;

public class PropertiesFileDemo {

  public static void main(String[] args) {

    String propertiesFileName = "db.properties";
    String xmlPropertiesFile = "db.xml";
    String classpathPropertiesFile = "database.properties";
    String xmlClasspathPropertiesFile = "database.xml";

    readPropertiesFile(propertiesFileName, xmlPropertiesFile);
    System.out.println();
    readFilesFromClasspath(classpathPropertiesFile, xmlClasspathPropertiesFile);
    System.out.println();
    //assign default value for unavailable key in properties file
    assignDefaultValue(propertiesFileName);
    System.out.println();
    //get all keys from properties file
    readAllKeys(propertiesFileName, xmlPropertiesFile);
    System.out.println();
    writePropertiesFile(propertiesFileName, xmlPropertiesFile);
  }


  private static void readPropertiesFile(String propertiesFileName, String xmlPropertiesFile) {
    System.out.println("**********Reading properties file**************");
    Properties properties = new Properties();
    try {
      FileReader fileReader = new FileReader(propertiesFileName);

      properties.load(fileReader);

      System.out.println("User Name: " + properties.getProperty("db.username"));
      System.out.println("Password: " + properties.getProperty("db.password"));
      System.out.println("Url: " + properties.getProperty("db.url"));
      System.out.println("Driver: " + properties.getProperty("db.driver"));
      System.out.println("*******End of reading normal properties file*******");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    properties.clear();
    System.out.println("*******Reading xml properties file**************");
    try {
      InputStream inputStream = new FileInputStream(xmlPropertiesFile);

      try {
        properties.loadFromXML(inputStream);
        System.out.println("User Name: " + properties.get("db.username"));
        System.out.println("Password: " + properties.getProperty("db.password"));
        System.out.println("Url: " + properties.getProperty("db.url"));
        System.out.println("Driver: " + properties.getProperty("db.driver"));
        System.out.println("*******End reading xml properties file*******");
      } catch (IOException e) {
        e.printStackTrace();
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  private static void readFilesFromClasspath(String classpathPropertiesFile,
      String xmlClasspathPropertiesFile) {
    System.out.println("*****Reading properties file from classpath******");

    Properties properties = new Properties();
    InputStream inputStream = PropertiesFileDemo.class
        .getResourceAsStream("/" + classpathPropertiesFile);
    try {
      properties.load(inputStream);
      System.out.println("User Name: " + properties.getProperty("db.username"));
      System.out.println("Password: " + properties.getProperty("db.password"));
      System.out.println("Url: " + properties.getProperty("db.url"));
      System.out.println("Driver: " + properties.getProperty("db.driver"));
      System.out.println("*******End of reading normal properties file from classpath*******");
    } catch (IOException e) {
      e.printStackTrace();
    }

    properties.clear();
    System.out.println("*******Reading xml properties file from classpath**********");
    inputStream = PropertiesFileDemo.class.getResourceAsStream("/" + xmlClasspathPropertiesFile);

    try {
      properties.loadFromXML(inputStream);
      System.out.println("User Name: " + properties.getProperty("db.username"));
      System.out.println("Password: " + properties.getProperty("db.password"));
      System.out.println("Driver: " + properties.getProperty("db.driver"));
      System.out.println("Url: " + properties.getProperty("db.url"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static void assignDefaultValue(String propertiesFileName) {
    System.out.println("**********Reading properties file**************");
    Properties properties = new Properties();
    try {
      FileReader fileReader = new FileReader(propertiesFileName);

      properties.load(fileReader);

      System.out.println("User Name: " + properties.getProperty("db.username"));
      System.out.println("Password: " + properties.getProperty("db.password"));
      //value is not available corresponding to db.host so default value localhost will print
      System.out.println("Host: " + properties.getProperty("db.host", "localhost"));
      System.out.println("*******End of reading normal properties file*******");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  private static void readAllKeys(String propertiesFileName, String xmlPropertiesFile) {
    System.out.println("*****Reading properties file for all keys******");
    Properties properties = new Properties();
    try {
      FileReader reader = new FileReader(propertiesFileName);
      properties.load(reader);
      Set<Object> allKeys = properties.keySet();
      for (Object key : allKeys) {
        System.out.println(key + " : " + properties.getProperty((String) key));
      }
      System.out.println("***End of reading normal properties file for all keys***");
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    properties.clear();
    System.out.println("***Reading xml properties file for all keys***");
    try {
      InputStream inputStream = new FileInputStream(xmlPropertiesFile);
      try {
        properties.loadFromXML(inputStream);
        Set<Object> allKeys = properties.keySet();
        for (Object key : allKeys) {
          System.out.println(key + " : " + properties.getProperty((String) key));
        }
        System.out.println("***End of reading xml properties file for all keys***");
      } catch (IOException e) {
        e.printStackTrace();
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
  }

  private static void writePropertiesFile(String propertiesFileName, String xmlPropertiesFile) {
    System.out.println("********Stating of writing properties file********");
    Properties properties = new Properties();
    properties.setProperty("db.host", "localhost");
    properties.setProperty("db.username", "system");
    properties.setProperty("db.password", "password");
    try {
      properties.store(new FileWriter(propertiesFileName), "db configuration file");
      System.out.println(propertiesFileName + " has written successfully");

      properties.storeToXML(new FileOutputStream(xmlPropertiesFile), "db configuration XML file");
      System.out.println(xmlPropertiesFile + " has written successfully");
      System.out.println("******end of writing properties files*********");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
